#!/usr/bin/ruby

require 'net/http'
require 'uri'
require 'json'

def main
    if res = doLoginCorreios(getLoginForm, 'cspo9438', 'cris9438')
        puts res
        puts "Entrou nos Correios"
        # doSearch "LX006534269CN"
    

        # foreach pedido do doSearch
    else
        puts "Falha ao entrar nos correios"
    end

    # doLoginRastreio("contas@presentesdomundo.com", "Pdomundo8412**")
end


def endString(s)
    l = s.index(" />") - 1
    return s[0, l] if l
end

def getLoginForm
    res = getProxy("https://apps.correios.com.br/cas/login?service=https%3A%2F%2Fapps.correios.com.br%2Fportalimportador%2Fpages%2FpesquisarRemessaImportador%2FpesquisarRemessaImportador.jsf")

    ltField = "<input type=\"hidden\" name=\"lt\" value=\""
    executionField = 'input type="hidden" name="execution" value="'

    if res.body.include?(executionField) && res.body.include?(ltField)
        indexLT = res.body.index(ltField) + ltField.length
        indexExecution = res.body.index(executionField) + executionField.length 

        lt = endString(res.body[indexLT, 55])
        execution = endString(res.body[indexExecution, 5500])

        return {
            'lt' => lt,
            'execution' => execution
        }
    end

    return false
end

def doLoginCorreios(form, user, pass)
    # Main Req
    uri = URI.parse("https://apps.correios.com.br/cas/login?service=https%3A%2F%2Fapps.correios.com.br%2Fportalimportador%2Fpages%2FpesquisarRemessaImportador%2FpesquisarRemessaImportador.jsf")
    request = Net::HTTP::Post.new(uri)  
    request.set_form_data(
        "_eventId" => "submit",
        "execution" => form['execution'],
        "lt" => form['lt'],
        "password" => pass,
        "username" => user
    )
    response = useProxy(uri, request)

    #Redir Login
    uri = URI.parse(response['Location'])
    reqLogin = Net::HTTP::Get.new(uri)
    resLogin = useProxy(uri, reqLogin)

    #Redir get page
    uri = URI.parse(resLogin['Location'])
    reqPage = Net::HTTP::Get.new(uri)
    resPage = useProxy(uri, reqPage)

    

    return true if resPage.code === "200" || resPage.code === "301"
end

def doSearch(form, code)
    # form['a']
    # form['b']
    # form['view']
    
    uri = URI.parse("https://apps.correios.com.br/portalimportador/pages/pesquisarRemessaImportador/pesquisarRemessaImportador.jsf?cid=7")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded;charset=UTF-8"
    request.set_form_data(
        "form-pesquisarRemessas" => "form-pesquisarRemessas",
        "form-pesquisarRemessas:codigoEncomenda" => code,
        "form-pesquisarRemessas:" + "j_idt120:j_idt133" => "1",
        "form-pesquisarRemessas:" + "j_idt67:j_idt79" => "1",
        "javax.faces.ViewState" => "-845691235635781139:-1120614746770648064",
        "javax.faces.behavior.event" => "action",
        "javax.faces.partial.ajax" => "true",
        "javax.faces.partial.event" => "click",
        "javax.faces.partial.execute" => "form-pesquisarRemessas:btnPesquisar form-pesquisarRemessas",
        "javax.faces.partial.render" => "form-pesquisarRemessas",
        "javax.faces.source" => "form-pesquisarRemessas:btnPesquisar",
    )
    
    res = useProxy(uri, request)

    p res.inspect
end


# Deprecated
def doLoginRastreio(user, pass)
    
    
    uri = URI.parse("https://rastreio.empreender.com.br/Integracao/getLogin")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded; charset=UTF-8"
    request.set_form_data(
        "email" => user,
        "senha" => pass,
    )
        
    response = useProxy(uri, request)

    body =  JSON.parse(response.body)

    resRedir = getProxy(body['call'])

    res = getProxy resRedir['Location']

    p res.body

end

def getProxy(uri)
    uri = URI.parse(uri)
    req = Net::HTTP::Get.new(uri)

    return useProxy(uri, req)
end

def useProxy(uri, req)
    
    puts "Requesting - " + uri.to_s[0,50] + "..."
    proxy_uri = URI.parse('https://localhost:8888')
    proxy = Net::HTTP::Proxy(proxy_uri.host, proxy_uri.port)

    req_options = {
        use_ssl: true,
        verify_mode: OpenSSL::SSL::VERIFY_NONE
    }

    res = proxy.start(uri.hostname, uri.port, req_options) do |http|
        http.request(req)
    end

    return res
end

main
