#!/usr/bin/ruby

# require 'net/http'
# require 'uri'
require 'json'

require './lib/proxy'
require './lib/correios'
require './lib/shopify'

def main
    if res = doLoginCorreios(getLoginForm, 'cspo9438', 'cris9438')
        puts "Entrou nos Correios"

        doSearch(res, "LX006534269CN")
    

        # foreach pedido do doSearch
    else
        puts "Falha ao entrar nos correios"
    end

end


def endString(s)
    l = s.index('"')
    return s[0, l] if l
end

def searchTerm(body, search, length)
    if body.include?(search)
        index = body.index(search) + search.length
        return endString(body[index, length])
    end
end



def doLoginCorreios(form, user, pass)
    # Main Req
    uri = URI.parse("https://apps.correios.com.br/cas/login?service=https%3A%2F%2Fapps.correios.com.br%2Fportalimportador%2Fpages%2FpesquisarRemessaImportador%2FpesquisarRemessaImportador.jsf")
    request = Net::HTTP::Post.new(uri)  
    request.set_form_data(
        "_eventId" => "submit",
        "execution" => form['execution'],
        "lt" => form['lt'],
        "password" => pass,
        "username" => user
    )
    response = useProxy(uri, request)

    #Redir Login
    uri = URI.parse(response['Location'])
    reqLogin = Net::HTTP::Get.new(uri)
    resLogin = useProxy(uri, reqLogin)

    #Redir get page
    uri = URI.parse(resLogin['Location'])
    reqPage = Net::HTTP::Get.new(uri)
    resPage = useProxy(uri, reqPage)

    search = 'method="post" action="'
    puts searchTerm(resPage.body, search, 150)
    search = 'id="javax.faces.ViewState" value="'
    puts searchTerm(resPage.body, search, 50)


    # file = File.open('res.html', 'w')
    # file.puts resPage.body

    return true if resPage.code === "200" || resPage.code === "301"
end

def doSearch(form, code)
    
    uri = URI.parse("https://apps.correios.com.br/portalimportador/pages/pesquisarRemessaImportador/pesquisarRemessaImportador.jsf?cid=7")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded;charset=UTF-8"
    request.set_form_data(
        "form-pesquisarRemessas" => "form-pesquisarRemessas",
        "form-pesquisarRemessas:codigoEncomenda" => code,
        "javax.faces.ViewState" => form,
        "javax.faces.source" => "form-pesquisarRemessas:btnPesquisar",
        "javax.faces.partial.event" => "click",
        "javax.faces.partial.execute" => "form-pesquisarRemessas:btnPesquisar form-pesquisarRemessas",
        "javax.faces.partial.render" => "form-pesquisarRemessas",
        "javax.faces.behavior.event" => "action",
        "javax.faces.partial.ajax" => "true",
    )
        

    
    res = useProxy(uri, request)

    resRedir = getProxy(res['Location'])

    p resRedir
    # file = File.open('res.html', 'w')
    # file.puts res.body

    
end


# Deprecated
def doLoginRastreio(user, pass)
    
    
    uri = URI.parse("https://rastreio.empreender.com.br/Integracao/getLogin")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded; charset=UTF-8"
    request.set_form_data(
        "email" => user,
        "senha" => pass,
    )
        
    response = useProxy(uri, request)

    body =  JSON.parse(response.body)

    resRedir = getProxy(body['call'])

    res = getProxy resRedir['Location']

    p res.body

end



main
