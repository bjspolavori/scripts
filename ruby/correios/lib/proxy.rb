require 'net/http'
require 'uri'

def getProxy(uri)
    uri = URI.parse(uri)
    req = Net::HTTP::Get.new(uri)

    return useProxy(uri, req)
end

def useProxy(uri, req)
    
    puts "Proxing - " + uri.to_s[0,50] + "..."
    proxy_uri = URI.parse('https://localhost:8888')
    proxy = Net::HTTP::Proxy(proxy_uri.host, proxy_uri.port)

    req_options = {
        use_ssl: true,
        verify_mode: OpenSSL::SSL::VERIFY_NONE
    }

    res = proxy.start(uri.hostname, uri.port, req_options) do |http|
        http.request(req)
    end

    return res
end