# require './proxy'

def getLoginForm
    res = getProxy("https://apps.correios.com.br/cas/login?service=https%3A%2F%2Fapps.correios.com.br%2Fportalimportador%2Fpages%2FpesquisarRemessaImportador%2FpesquisarRemessaImportador.jsf")

    search = "<input type=\"hidden\" name=\"lt\" value=\""
    lt = searchTerm(res.body, search, 55)
    
    search = 'input type="hidden" name="execution" value="'
    execution = searchTerm(res.body, search, 5500)

    return {
        'lt' => lt,
        'execution' => execution
    }
end