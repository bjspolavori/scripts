require 'net/http'

def shopify(url, method, form = nil, use_proxy = false)
    uri = URI("https://presentesdomundo2.myshopify.com/admin/api/#{$API_VERSION}/" + url)
    
    req = Net::HTTP::Get.new(uri) if method == 'GET'
    req = Net::HTTP::Post.new(uri) if method == 'POST'
    req = Net::HTTP::Put.new(uri) if method == 'PUT'
    
    if form
        req['Content-Type'] = 'application/json'
        req.body = form
    end
    
    req['X-Shopify-Access-Token'] = ENV['SHOPIFY_TOKEN']
    req['Host'] = 'presentesdomundo2.myshopify.com'
    
    req_options = {
        use_ssl: true,
        verify_mode: use_proxy ? OpenSSL::SSL::VERIFY_NONE : OpenSSL::SSL::VERIFY_PEER
    }
    
    if use_proxy
        puts 'Proxing - ' + uri.to_s[0, 50] + '...'
        
        proxy_uri = URI.parse('https://localhost:8888')
        proxy = Net::HTTP::Proxy(proxy_uri.host, proxy_uri.port)
        
        res = proxy.start(uri.host, uri.port, req_options) do |http|
            http.request req
        end
    else
        res = Net::HTTP.start(uri.host, uri.port, req_options) do |http|
            http.request req
        end
    end
    res
end

def namesToIds(orders)
    ids = []
    orders.each do |order|
        
        res = use_api('orders.json?status=any&fields=id&name=' + order[:id], 'GET')
        data = JSON.parse(res.body)['orders']
        
        if data && data.length > 0
            id = data[0]['id']
            
            puts "#{order[:id]} -> #{id}"
            
            order['id'] = id
            ids << order
        else
            puts 'Error on ' + order[:id]
            setError(order[:id] + ' Pedido já arquivado ou não encontrado')
        end
    end
  
    ids
end

