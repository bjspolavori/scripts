def nilTrack(orders)
    orders.each{ |order|
        order[:track] = -1
    }
end