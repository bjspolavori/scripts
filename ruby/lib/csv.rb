require 'csv'

$REPLACEMENTS = { 
  'á' => 'a',
  'ã' => 'a',
  'é' => 'e',
  'ê' => 'e',
  'ô' => 'o',
  'ó' => 'o'
}
$CSV_PARAMS = { col_sep: ';' }

def get_csv(path, id_field = 1, track_field = 20, date = false, isCheck = false, name_field: 4, zip_field: 9, quantity_field: 6, identity_field: 13)
  c = 0
  orders = []
  
  CSV.foreach(path, $CSV_PARAMS) do |line|
    next if date && date != line[0]  
    next if line[0] == "DATA" 
    next if line[0] == "orders" 
    
    next if line[1] == "" || line[1] == nil 
    if verify_id line[id_field]
      id = if line[id_field].include? '#'
             line[id_field][1, 6]
           else
             line[id_field]
           end
      case verify_track(line[track_field])
      when 'CORREIOS'
        track = line[track_field]
        # track_url = "https://t.17track.net/pt#nums=#{track}"
        # track_url = "https://www.linkcorreios.com.br/?id=#{track}"
        # track_url = "https://track.ebanx.com/pt/t?trackingCode=#{track}"
        track_url = "https://rastreio.ninja/buscar/#{track}"
        company = 'Correios'

      when 'JADLOG'
        track = line[track_field]
        track_url = 'https://www.jadlog.com.br/tracking'
        company = 'Jadlog'
      else
        track = -1
        track_url = ''
        company = ''
      end
  
      c += 1
      identity = line[identity_field] ? line[identity_field][1,12] : nil
      # line = { 'id': id, track: track, trackUrl: track_url, company: company, name: line[name_field], zip: line[zip_field], quantity:line[quantity_field], identity:line[identity_field][1,12]} 
      line = { 'id': id, track: track, trackUrl: track_url, company: company, name: line[name_field], zip: line[zip_field], quantity:line[quantity_field], identity: identity} 
      orders << line if isCheck || track != -1
      puts JSON.dump(line)
    end
  end

  puts "CSV read #{c} entries"
  puts "Revise as informações acima, se algo estiver errado saia com CTRL + C"
  puts "Tecle ENTER para continuar a execução"
  STDIN.gets
  return orders
end

def get_orders_csv(path, id_field = 0)
  orders = []
  CSV.foreach(path) do |line|
      if verify_id line[id_field]
          id = if line[id_field].include? '#'
              line[id_field][1, 6]
          else
              line[id_field]
          end
          orders << id
      end
  end
  return orders
end

def verify_id(id)
  if id
    if id.length > 4
      return true
    end
  end
  puts 'CSV => Verify ID ( No valid ID )'
  return false
end

def verify_track(track)
  if track
    if track.length == 13
      return 'CORREIOS'
    elsif track.length == 14
      return 'JADLOG'
    end
  end
  return false
end

def rewrite_csv(file, data)
  old_csv = CSV.read(file, $CSV_PARAMS)
  csv = []

  old_csv.each{ |order|
    data.each{ |d|
      if d[:id].to_s == order[1].to_s || "##{d[:id].to_s}" == order[1].to_s
        
        order[12] = d[:track] if d[:track] != -1
        if d[:shipping] 

          order[2] = d[:quantity].to_i * 33 
          order[3] = "2,9"
          order[4] = d[:shipping]
          subTotal = order[2].to_f + order[3].sub(',', '.').to_f + order[4].sub(',', '.').to_f
          order[5] = subTotal.to_s.sub('.', ',')
        end
      end
    }
  }
  
  return CSV.open(file, 'wb', $CSV_PARAMS) { |csv| old_csv.each { |row| csv << row }}
end

def delete_if_exist(file)
  begin 
    f = File.open(file, 'r') if File.exists? file
  ensure
    f.close unless f.nil? or f.closed?
    File.delete(file) if File.exists? file
  end
end

def normalizeText(text)
  if text
      text.split('').each { |c|
          c = $REPLACEMENTS[c] if $REPLACEMENTS.include? c
      }
      return "#{text}"
  end
end