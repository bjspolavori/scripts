def blingImportOrdersParam(orders)

    k = 0
    objs = "<xjxobj>"
    orders.each{ |order|
        objs += "<e><k>#{k}</k><v><xjxobj><e><k>numeroPedido</k><v>#{order['id']}</v></e><e><k>jsonPedido</k><v>undefined</v></e><e><k>strJson</k><v>undefined</v></e></xjxobj></v></e>"
        k += 1
        
    }
    
    result = objs+"</xjxobj>"
    path = "Pedidos #{ARGV[1]} Bling"
    File.open(path, 'a') { |file| file << result +"\n"}
    return result
end