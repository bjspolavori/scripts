## REF
# https://coderwall.com/p/c-mu-a/http-posts-in-ruby

def portalPostalImportLabelsFromCsv(file, auth_card)
    uri = URI.parse("https://portalpostal.com.br/rest/secure/importar-pre-postagem/upload-layout-cliente/")
    
    post_body = []
    boundary = "0xAAA"
    
    filter = {
        "modeloArquivo" => "NEW EXPORTER CREME",
        "servico" => "ARQUIVO",
        "idDepartamento" => 0,
        "departamento" => "SEM DEPARTAMENTO",
        "nomeUsuario" => "marcelo",
        "adicionais" => "",
        "vdPreVenda" => ""
    }
    
    request = setHeaders(uri, auth_card, content_type: "multipart/form-data; boundary=#{boundary}")
    
    
    request = Net::HTTP::Post.new(uri)
    # request["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYXJjZWxvIiwiZW1wcmVzYS5jbnBqIjoiOTQzMTI2MzQwMDAxMjMiLCJpZFVzdWFyaW8iOjE4NTEsImlkQ2xpZW50ZSI6NDI0MSwibG9naW4iOiJtYXJjZWxvIiwiaWF0IjoxNjExNjk4MjAyfQ.OHBzbOd5BYkbUoHKx02J_Rm4U2cFa9hCR33zri7aHjc"
    # request["Empresa.Cnpj"] = "94312634000123"
    # request["Agencia.Cep"] = "90230-002"
    # request["Idcliente"] = "4241"
    # request["Login"] = "marcelo"
    
    request["Host"] = "portalpostal.com.br"
    request["Sec-Ch-Ua-Mobile"] = "?0"
    request["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36"
    request["Sec-Ch-Ua"] = "\"Chromium\";v=\"88\", \"Google Chrome\";v=\"88\", \";Not A Brand\";v=\"99\""
    request["Accept"] = "*/*"
    request["Origin"] = "https://portalpostal.com.br"
    request["Sec-Fetch-Site"] = "same-origin"
    request["Sec-Fetch-Mode"] = "cors"
    request["Sec-Fetch-Dest"] = "empty"
    
    post_body << "--#{boundary}\r\n"
    post_body << "Content-Disposition: form-data; name=\"file\"; filename=\"#{File.bsename(file)}\"\r\n"
    # post_body << "Content-Type: application/octet-stream \r\n\r\n"
    post_body << "Content-Type: #{MIME::Types.type_for(file)} \r\n\r\n"
    post_body << File.read(file)
    
    post_body << "--#{boundary}\r\n"
    post_body << "Content-Disposition: form-data; name=\"filtro\""
    post_body << "\r\n\r\n--#{boundary}--\r\n"
    
    request.body = post_body.join
    
    response = generic_request(uri, request)
    puts response.body
end
