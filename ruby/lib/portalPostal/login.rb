def doLogin(user, pass, use_proxy: false)
    uri = URI.parse("https://portalpostal.com.br/rest/cliente/usuario/login?idEmpresa=90")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request["Host"] = "portalpostal.com.br"
    request["Accept"] = "application/json, text/plain, */*"
    request["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"
    request["Origin"] = "https://portalpostal.com.br"
    request["Referer"] = "https://portalpostal.com.br/v2/extra/login"
    request.body = JSON.dump({
      "login" => user,
      "senha" => pass
    })

    response = generic_request(uri, request, use_proxy: use_proxy)
    
    puts "Login return #{response.code} code"
    return JSON.parse(response.body)
end

def getLoginData(user, pass)
    data = doLogin(user, pass)['data']
    
    # token = "Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJtYXJjZWxvIiwiZW1wcmVzYS5jbnBqIjoiOTQzMTI2MzQwMDAxMjMiLCJpZFVzdWFyaW8iOjE4NTEsImlkQ2xpZW50ZSI6NDI0MSwibG9naW4iOiJtYXJjZWxvIiwiaWF0IjoxNjA5MDE1MjQzfQ.MuSqjn4yCZ9-z7ZAvITerOShDxytwlrcsv6hXO5yrqQ"
    token = "Bearer " + data['cliente_usuario']['token']
    cnpj_agencia = "#{data['cliente_usuario']['cnpjAgencia']}"
    id_usuario = "#{data['cliente_usuario']['clienteUsuario']['id']}"
    id_cliente = "#{data['cliente_usuario']['clienteUsuario']['idCliente']}"

    return {
        'Authorization' => token,
        'EmpresaCNPJ' => cnpj_agencia,
        'Idusuario' => id_usuario,
        'Idcliente' => id_cliente,
        'Login' => user
    }
end