def generic_request(uri, request, use_proxy: false)
     
    req_options = {
        use_ssl: uri.scheme == "https",
        verify_mode: use_proxy ? OpenSSL::SSL::VERIFY_NONE : OpenSSL::SSL::VERIFY_PEER
    }

    if use_proxy
        proxy_uri = URI.parse('https://localhost:8888')
        proxy = Net::HTTP::Proxy(proxy_uri.host, proxy_uri.port)
        
        response = proxy.start(uri.host, uri.port, req_options) do |http|
            http.request request
        end
    else
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
            http.request request
        end
    end

    return response
end


    #TODO: change to createRequestObject
def setHeaders(uri, auth_card, method: "get",  headers: {}, content_type: "application/json")
    # TODO: use for all http methods
    case method.downcase
    when "get"
        request = Net::HTTP::Get.new(uri, headers)
    when "post"
        request = Net::HTTP::Post.new(uri, headers)
    when "put"
        request = Net::HTTP::Put.new(uri, headers)
    end
    
    request.content_type = content_type

    request["Authorization"] = auth_card['Authorization']
    request["empresa.cnpj"] = auth_card['EmpresaCNPJ']
    request["idUsuario"] = auth_card['Idusuario']
    request["idCliente"] = auth_card['Idcliente']
    request["Login"] = auth_card['Login'] 
  

    #TODO: move out this func
    request["Host"] = "portalpostal.com.br"
    request["Sec-Ch-Ua"] = "\"Google Chrome\";v=\"87\", \" Not;A Brand\";v=\"99\", \"Chromium\";v=\"87\""
    request["Pragma"] = "no-cache"
    request["Sec-Ch-Ua-Mobile"] = "?0"
    request["Accept"] = "application/json, text/plain, */*"
    request["Cache-Control"] = "no-cache"
    request["User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"
    request["Expires"] = "Sat, 01 Jan 2000 00:00:00 GMT"
    request["Origin"] = "https://portalpostal.com.br"
    request["Sec-Fetch-Site"] = "same-origin"
    request["Sec-Fetch-Mode"] = "cors"
    request["Sec-Fetch-Dest"] = "empty"
    request["Accept-Language"] = "pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7"
     
    return request
end
