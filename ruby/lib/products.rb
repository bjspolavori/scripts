def filterProd(orders, prod)
    count = 0
    case prod
    when 'magical'
        prodTitle = [
            # 'TRATAMENTO AVANÇADO - MAGICAL HAIR™ 120ML',
            # 'MH PROMO', 
            'Máscara de tratamento avançado - MAGICAL HAIR™ 120ML', 
            'PROMO UNICA  MAGICAL HAIR™ 120ML', 
            # "TRATAMENTO AVANÇADO - MAGICAL HAIR™ - 3 produtos(59,9 cada)",
            # "TRATAMENTO AVANÇADO - MAGICAL HAIR™ - 1 produto(79,9)",
            # "TRATAMENTO AVANÇADO - MAGICAL HAIR™",
            # "Magical Hair » TRATAMENTO AVANÇADO - TRATAMENTO AVANÇADO"
        ]
    when 'chapinha'
        prodTitle = ['Chapinha Profissional - ALISSHAIR', 'Chapinha Profissional - ALISSHAIR [PROMOÇÃO BLACK FRIDAY]']
    when 'pistola'
        prodTitle = [
            'Pistola Massageadora Phoenix Professional 2.0 ® - 4 Ponteiras',
            'Pistola Massageadora Phoenix Professional 2.0 ® - 6 Ponteiras',
            'Pistola Massageadora Phoenix 3.0 - 6 Ponteiras'
        ]
    when 'vitor'
        prodTitle = [
            'Rodo Limpa e Seca Fácil Multi-funcional 4 em 1', 
            'Lixa de Pé Esfoliante Eletrônica - frete grátis', 
            'Lixa de Pé Esfoliante Eletrônica  - frete grátis',
            'TONIFICADOR MUSCULAR PARA GLÚTEO ELÉTRICO - Fit Toner'
        ]
    when 'shopee'
        prodTitle = [
            'Sutiã Adesivo Invisível Reutilizável [Frete grátis]',
            'Camiseta Smart Fitness - Tecnologia Efeito Sauna',
            'Camiseta Queima5X - Tecnologia Efeito Sauna',
            'Depilador Elétrico Depilax - FRETE GRÁTIS',
            'Kit 3 Cápsulas Reutilizáveis para Dolce Gusto',
            'Anti Peste Ultrassônico 2.0',
            'DepilMax™ Depilador de Sobrancelhas indolor - Frete Gratis', 
            'Caneta Blue Laser - Removedor de Espinhas'
        ]
    end
    
    filtred = Array.new
    orders.each{ |order|
        order['line_items'].each{ |prod|
            puts "#{order['name']} - #{prod["title"]}"
            if prodTitle
                if prodTitle.include? prod['title']
                    filtred << order 
                    count += 1
                end
            end
        }
    }
    puts "#{count} Produtos selecionados"   
    return filtred
end

def magicalString(res)
    sedex = "PAC"
    if res['shipping_lines'][0]
        sedex = "SEDEX" if res['shipping_lines'][0]['title'] == "FRENET_ENTREGA_EXPRESSA_03220"
    end
    d = res['created_at'].split('T')[0]
    return "#{d};#{res['name']};#{res['valorUnit']};#{res['qtd']};#{res['shipping_address']['name']};\"#{res['shipping_address']['company']}\";\"#{res['shipping_address']['zip']}\";#{res['shipping_address']['address1']};#{res['shipping_address']['address2']};#{res['shipping_address']['city']};#{res['shipping_address']['province_code']};#{res['shipping_address']['phone']};;#{res['name']} - #{res['line_items'][0]['title']} - #{res['qtd']};#{sedex}"
end

def chapinhaString(res)
    d = res['created_at'].split('T')[0]

    return "#{d};#{res['name']};#{res['line_items'][0]['quantity']};#{res['line_items'][0]['title']};#{res['shipping_address']['name']};#{res['shipping_address']['address1']};#{res['shipping_address']['address2']};\"#{res['shipping_address']['company']}\";#{res['shipping_address']['city']};\"#{res['shipping_address']['zip']}\";#{res['shipping_address']['province_code']};BR;+55 51 9553-7788;;"
end
def pistolaString(res)
    d = res['created_at'].split('T')[0]
    return "#{d};#{res['name']};#{res['line_items'][0]['quantity']};#{res['line_items'][0]['title']};#{res['line_items'][0]['variant_title']};#{res['shipping_address']['name']};#{res['shipping_address']['address1']};#{res['shipping_address']['address2']};\"#{res['shipping_address']['company']}\";#{res['shipping_address']['zip']};\"#{res['shipping_address']['city']}\";#{res['shipping_address']['province_code']};BR;+55 51 9553-7788;;"
end

def shopeeString(res)
    return "#{res['name']};#{res['line_items'][0]['title']};#{res['qtd']};#{res['shipping_address']['name']};\"#{res['shipping_address']['phone']}\";\"#{res['shipping_address']['zip']}\";#{res['shipping_address']['address1']};#{res['shipping_address']['address2']};"
end

def vitorString(res)
    address = res['shipping_address']['address1']
    address = address.gsub(',',';')
    address = "#{address};" if address.count(';') < 2
    # address = address.gsub(' ','') # Remove space before complement
    # PEDIDO	DATA	PRODUTO	PREÇO	CLIENTE	RUA	NUMERO	COMPLEMENTO	BAIRRO	CIDADE	ESTADO	CEP	RASTREIO														
    return "#{res['name']};;#{res['line_items'][0]['title']};#{res['qtd']};#{res['shipping_address']['name']};#{address};#{res['shipping_address']['address2']};#{res['shipping_address']['city']};#{res['shipping_address']['province_code']};\"#{res['shipping_address']['zip']}\";"
end

def rastreioString(res)
    return "#{res['name']};#{res['line_items'][0]['track_number']};#{res['shipping_address']['name']}"
end

def allString(res)
    return "#{res['name']};#{res['line_items'][0]['title']};#{res['shipping_address']['name']};\"#{res['shipping_address']['company']}\""
end