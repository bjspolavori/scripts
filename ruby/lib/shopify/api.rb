require 'net/http'

$API_VERSION = "2020-04"

def use_api(url, method, form = nil, use_proxy = false)
  sleep 1
  uri = URI("https://presentesdomundo2.myshopify.com/admin/api/#{$API_VERSION}/" + url)

  req = Net::HTTP::Get.new(uri) if method == 'GET'
  req = Net::HTTP::Post.new(uri) if method == 'POST'
  req = Net::HTTP::Put.new(uri) if method == 'PUT'

  if form
    req['Content-Type'] = 'application/json'
    req.body = form
  end

  req['X-Shopify-Access-Token'] = ENV['SHOPIFY_TOKEN']
  req['X-Shopify-Access-Token'] = "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"
  req['Host'] = 'presentesdomundo2.myshopify.com'

  req_options = {
    use_ssl: true,
    verify_mode: use_proxy ? OpenSSL::SSL::VERIFY_NONE : OpenSSL::SSL::VERIFY_PEER
  }

    if use_proxy
      puts 'Proxing - ' + uri.to_s[0, 50] + '...'

      proxy_uri = URI.parse('https://localhost:8888')
      proxy = Net::HTTP::Proxy(proxy_uri.host, proxy_uri.port)

      res = proxy.start(uri.host, uri.port, req_options) do |http|
        http.request req
      end
    else
      res = Net::HTTP.start(uri.host, uri.port, req_options) do |http|
        http.request req
      end
    end
    
  res
end

def namesToIds(orders)
  ids = []
  orders.each do |order|
    sleep 2
    
    res = use_api('orders.json?status=any&fields=id&name=' + order[:id], 'GET', nil)
    puts res.code
    data = JSON.parse(res.body)['orders']

    if data && data.length > 0
      id = data[0]['id']

      puts "#{order[:id]} -> #{id}"

      order['id'] = id
      ids << order
    else
      puts 'Error on ' + order[:id]
      # setError(order[:id] + ' Pedido já arquivado ou não encontrado')
    end
  end

  ids
end

def get_order_to_process(date)
  startDate = date - 90
  puts startDate
  # NAO ALTERE DE FORMA NENHUMA
  url = "orders.json?financial_status=paid&limit=250&fulfillment_status=any&fields=id,name,line_items&created_at_min=#{startDate}T00:00:00-00:00&created_at_max=#{date}T00:00:00-00:00"
  # NAO ALTERE DE FORMA NENHUMA
  res = use_api(url, "GET");

  return JSON.parse(res.body)['orders']
end