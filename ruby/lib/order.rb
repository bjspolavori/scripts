def getOrderInfo(id, resend = [], extras = [])
    res = use_api('orders/' + id.to_s + '.json', 'GET')
    puts res.code
    res = JSON.parse(res.body)['order']
    
    tituloVariante = JSON.parse(res['line_items'].to_json)[0]['variant_title']
    titulo = JSON.parse(res['line_items'].to_json)[0]['title']
    quantidade = JSON.parse(res['line_items'].to_json)[0]['quantity']

    address1 = res['shipping_address']['address1']
    address = address1.split(',')[0]
    address1 = address1.gsub(/\s+/, "")
    address1 = address1.split(',')
    
    res['qtd'] = (useQuantity tituloVariante) ? quantidade : tituloVariante
    res['qtd'] = (extras.include? res['name']) ? res['qtd'].to_i + 1 : res['qtd'] if extras

    res['valorUnit'] = "19,90"
    res['resend'] = if (resend.include? res['name'][1,6].to_i) 
        "Reenvio" 
    else 
        titulo
    end

    case ARGV[1]
    when "magical"
        str = magicalString res 
    when "vitor"
        str = vitorString res 
    when "shopee"
        str = shopeeString  res
    when "chapinha"
        str = chapinhaString  res
    when "pistola"
        str = pistolaString  res
    when "rastreio"
        srt = rastreioString res
    else
        str = allString  res
    end

    return str
end

def addOrder(content, file)
    content = normalizeText content
    File.open(file, 'a') { |file| file << content +"\n"}
end

def addRessend(orders, resends)
    newOrders = Array.new
    resends.each{ |resend|
        newOrders << {id: resend.to_s}
    }
    newOrders = namesToIds newOrders
    orders.concat(newOrders)
end

def useQuantity(title)
    return true if !title
    excludeVariants = ["6 peças"]
    return excludeVariants.include? title
end