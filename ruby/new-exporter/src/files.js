const path = require("path");
const fs = require("fs");
const { Parser } = require("json2csv");

const outPathAbs = path.join(__dirname, "..", "out");

const writeToCsv = (file, text) => {
  if (!fs.existsSync(file)) fs.writeFileSync(file, "");
  let data = fs.readFileSync(file, "utf-8");
  const fd = fs.openSync(file, "w+");

  data = data.split("\n");
  data.shift();
  data = data.join("\n");

  const oldOrders = new Buffer.from(data);
  const newOrders = new Buffer.from(text + "\n");

  fs.writeSync(fd, newOrders, 0, newOrders.length, 0); //write new data
  fs.writeSync(fd, oldOrders, 0, oldOrders.length, newOrders.length); //append old data

  fs.close(fd, (e) => {
    if (e) console.log(e);
  });
};

const exportOrders = (orders) => {
  orders.forEach((order) => {
    try {
      console.log(order);
      const parser = new Parser({ fields: order.product.exportFields });
      const csv = parser.parse(order);
      writeToCsv(path.join(outPathAbs, order.product.fileName + ".csv"), csv);
    } catch (error) {
      console.error(error);
    }
  });
};

module.exports = {
  exportOrders,
};
