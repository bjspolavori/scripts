const beautifyDate = (date) => {
  date = date.split("T");
  date = date[0].split("-");
  date = date.reverse();
  date = date.join("/");
  return date;
};

module.exports = {
  beautifyDate,
};
