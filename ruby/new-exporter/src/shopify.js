const { beautifyDate } = require("./functions");

const axios = require("axios");

const shopifyGetPaidOrder = (query, order, callback) => {
  const data = JSON.stringify({
    // TODO treat all possibilities of this input
    query: typeof query !== String ? query(order) : query,
    variables: {},
  });

  var config = {
    method: "post",
    url:
      "https://presentesdomundo2.myshopify.com/admin/api/2020-10/graphql.json",
    headers: {
      "X-Shopify-Access-Token": "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",
      "Content-Type": "application/json",
    },
    data: data,
  };

  return axios(config)
    .then((res) => shopifyFormatOrderResponse(res))
    .then((res) => (res ? res.orders : res))
    .then((orders) => {
      if (typeof callback == "function") callback(orders);
      return orders;
    })
    .catch((error) => {
      console.error(error);
    });
};

const shopifyFormatOrderResponse = (response) => {
  // If need to update last order filter, set to true
  if (false)
    console.log(
      response.data.data.orders.edges[0].node.name,
      response.data.data.orders.edges[0].cursor
    );
  if (response.data.errors) {
    console.log(response.data.errors[0]);
  }
  if (!response) return null;
  if (!response.data) return null;
  if (!response.data.data) return null;
  if (!response.data.data.orders) return null;
  if (!response.data.data.orders.edges) return null;
  if (!response.data.data.orders.edges[0]) return null;

  const { data, extensions } = response.data;
  const queryCost = extensions.cost.requestedQueryCost;

  const ordersData = new Array();

  for (let j = 0; j < data.orders.edges.length; j++) {
    const { node: order } = data.orders.edges[j];

    Object.keys(order.billingAddress).forEach((k) => {
      order[k] = order.billingAddress[k];
    });
    delete order.billingAddress;

    for (let i = 0; i < order.lineItems.edges.length; i++) {
      const { node } = order.lineItems.edges[i];
      order.lineItems[i] = node;
    }
    delete order.lineItems.edges;

    order.createdAt = beautifyDate(order.createdAt);

    ordersData.push(order);
  }

  return {
    orders: ordersData,
    queryCost: queryCost,
  };
};

module.exports = {
  shopifyGetPaidOrder,
};
