// TODO use it to route all products, must return an array
const path = require("path");
const fs = require("fs");

const getProductsFromDir = (dir) => {
  const data = new Array();

  const productsDir = path.join(__dirname, dir);
  const products = fs.readdirSync(productsDir);

  products.forEach((product) => {
    const productFileName = product.split(".", 1)[0];
    const prodPath = path.join(productsDir, productFileName);

    data[productFileName] = require(prodPath);
  });
  return data;
};

module.exports = getProductsFromDir("products");
