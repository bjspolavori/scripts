const { shopifyGetPaidOrder } = require("./src/shopify.js");
const { exportOrders } = require("./src/files");
const products = require("./products");

// TODO: Armazenar este valor em um registro redis
// TODO: Criar uma função que percorre os pedidos e retorna todos pagos e não processados
const REF_ORDER_CURSOR =
  "eyJsYXN0X2lkIjoxMzA3MjU1ODY1Mzc4LCJsYXN0X3ZhbHVlIjoiMjAyMC0wOS0xNSAyMjo0OToyNC4wMDAwMDAifQ==";

const getOrderQuery = `query: "11420", first:1`;
const getPaidOrdersQuery = `query: "status:open AND fulfillment_status:unfulfilled AND financial_status:paid", first:60, after:"${REF_ORDER_CURSOR}"`;
const productToExport = "creme";

const query = getPaidOrdersQuery;

queryFunc = (query) =>
  products[productToExport].getQuery(query).split(/\s+/).join(" ");

shopifyGetPaidOrder(queryFunc, query)
  .then((orders) => {
    const res = new Array();

    for (let i = 0; i < orders.length; i++) {
      let order = orders[i];

      for (order of shopifyDivideOrderByLineItem(order)) {
        order = assingOrderToProduct(order, products);

        if (order) res.push(order);
      }
    }
    return res;
  })
  .then((orders) => exportOrders(orders));

const shopifyDivideOrderByLineItem = (order, products) => {
  const res = new Array();

  for (let i = 0; order.lineItems && order.lineItems[i]; i++) {
    const item = order.lineItems[i];
    const tempOrder = order;

    tempOrder.item = item.title;
    tempOrder.quantity = item.quantity;
    delete tempOrder.lineItems;

    tempOrder.customer = tempOrder.firstName + tempOrder.lastName;
    delete tempOrder.firstName;
    delete tempOrder.lastName;

    tempOrder.province = tempOrder.provinceCode;
    delete tempOrder.provinceCode;

    res.push(tempOrder);
  }
  return res;
};

const assingOrderToProduct = (order, products) => {
  for (const [key, value] of Object.entries(products)) {
    let product = value;
    if (product.productNames.includes(order.item)) {
      order.product = product;
      return order;
    }
  }
};
