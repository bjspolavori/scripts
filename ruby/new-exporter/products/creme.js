const getQuery = (params) => {
  const query = `
  {
    orders(${params}) {
      edges {
        node {
          name
          createdAt
          billingAddress{
              address1
              address2
              city
              company
              firstName
              lastName
              phone
              provinceCode
              zip
          }
          lineItems(first: 10){
            edges{
              node{
                title
                name
                quantity
                variantTitle
              }
            }
          }
        }
      }
    }
  }`;
  return query;
};
const productNames = [
  "Magical Hair",
  "Máscara de tratamento avançado - MAGICAL HAIR™ 120ML",
];
const exportFields = [
  "createdAt",
  "name",
  "quantity",
  "customer",
  "company",
  "zip",
  "address1",
  "address2",
  "city",
  "province",
  "phone",
];
// campos para etiqueta NOME, CEP, ENDEREÇO, NUMERO, BAIRRO, CIDADE, UF, CPF
const fileName = "Pedidos Creme";

module.exports = {
  productNames,
  getQuery,
  exportFields,
  fileName,
};
