const getQuery = (params) => {
  const query = `
  {
    orders(${params}) {
      edges {
        node {
          name
          createdAt
          billingAddress{
              address1
              address2
              city
              company
              firstName
              lastName
              phone
              provinceCode
              zip
          }
          lineItems(first: 10){
            edges{
              node{
                title
                quantity
              }
            }
          }
        }
      }
    }
  }`;
  return query;
};
const productNames = [
  "Chapinha Alisshair",
  "Chapinha",
  "Chapinha Profissional - ALISSHAIR",
  "Chapinha Profissional - ALISSHAIR [Black Friday]", //? verificar este nome
];

const exportFields = [
  "createdAt",
  "name",
  "quantity",
  "item",
  "customer",
  "address1",
  "address2",
  "city",
  "company",
  "phone",
  "province",
];
const fileName = "Pedidos Chapinha";

module.exports = {
  productNames,
  getQuery,
  exportFields,
  fileName,
};
