const getQuery = (params) => {
  const query = `
  {
    orders(${params}) {
      edges {
        node {
          name
          createdAt
          billingAddress{
              address1
              address2
              city
              company
              firstName
              lastName
              phone
              provinceCode
              zip
          }
          lineItems(first: 10){
            edges{
              node{
                title
                quantity
              }
            }
          }
        }
      }
    }
  }`;
  return query;
};
const productNames = [
  "Pistola Massageadora Phoenix Professional 2.0 ® - 4 Ponteiras",
];

const exportFields = [
  "createdAt",
  "name",
  "quantity",
  "item",
  "customer",
  "address1",
  "address2",
  "city",
  "company",
  "phone",
  "province",
];

const fileName = "Pedidos Pistola";

module.exports = {
  productNames,
  getQuery,
  exportFields,
  fileName,
};
