const getQuery = (params) => {
  const query = `
  {
    orders(${params}) {
      edges {
        cursor
        node {
          id
          name
          createdAt
          billingAddress{
              address1
              address2
              city
              company
              firstName
              lastName
              phone
              provinceCode
              zip
          }
          lineItems(first: 10){
            edges{
              node{
                title
              }
            }
          }
        }
      }
    }
  }`;
  return query;
};
const productNames = ["Magical Hair", "Creme"];
const exportFields = ["createdAt", "name"];

const fileName = "Pedidos Outros";

module.exports = {
  productNames,
  getQuery,
  exportFields,
  fileName,
};
