const getQuery = (order) => {
  const query = `{
    orders(query: "${order} AND financial_status:paid", first: 1) {
        edges {
          node {
            name
            createdAt
            billingAddress{
                address1
                address2
                city
                company
                firstName
                lastName
                phone
                provinceCode
                zip
            }
            lineItems(first: 10){
                edges{
                    node{
                        title
                    }
                }
            }
          }
        }
    }
  }`;
  return query;
};
const productNames = [
  "Magical Hair",
  "TRATAMENTO AVANÇADO - MAGICAL HAIR™ 120ML » 1",
];
const exportFields = ["createdAt", "name", "firstName"];
module.exports = {
  exportFields,
  getQuery,
  productNames,
};
